"""
Documentation
"""
# Python Modules
import argparse
import enum
import logging
import time

from functools import partial

# 3rd Party Modules
import hyperopt

from hyperopt import hp, Trials
from hyperopt.mongoexp import MongoTrials

# Project Modules
from objective import rosenbrock, eggholder


class Algorithm(enum.Enum):
    RANDOM = 'random', hyperopt.rand.suggest
    TPE = 'tpe', hyperopt.tpe.suggest

    def __new__(cls, keycode, searcher):
        obj = object.__new__(cls)
        obj._value_ = keycode
        obj.searcher = searcher

        return obj

    def __str__(self):
        return self.value


class Objective(enum.Enum):
    ROSENBROCK = (
        'rosenbrock',
        rosenbrock,
        {
            'x{}'.format(i): hp.uniform('x{}'.format(i), -100, 100)
            for i in range(20)
        }
    )

    EGGHOLDER = (
        'eggholder',
        eggholder,
        {
            'x': hp.uniform('x', -512, 512),
            'y': hp.uniform('y', -512, 512)
        }
    )

    def __new__(cls, keycode, fn, space):
        obj = object.__new__(cls)
        obj._value_ = keycode
        obj.fn = fn
        obj.space = space

        return obj

    def __str__(self):
        return self.value


def main(args):
    logging.basicConfig(
        level=logging.DEBUG,
        format='%(asctime)-15s [%(name)s]:%(lineno)d %(levelname)s %(message)s',
        filename=args.log_file,
        filemode='w'
    )
    logging.getLogger('hyperopt.mongoexp').setLevel(logging.WARNING)

    mongo_path = 'mongo://localhost:27017/{}/jobs'.format(args.database)
    trials = MongoTrials(mongo_path) if args.parallel else Trials()

    if args.algorithm == Algorithm.TPE:
        algo = partial(
            args.algorithm.searcher,
            n_EI_candidates=1000,
            gamma=0.2,
            n_startup_jobs=args.startup_jobs
        )
    else:
        algo = args.algorithm.searcher

    start = time.time()
    best = hyperopt.fmin(
        fn=args.objective.fn,
        space=args.objective.space,
        algo=algo,
        max_evals=args.max_evals,
        trials=trials
    )
    elapsed = time.time() - start

    log = logging.getLogger(__name__)
    log.info("It took %f seconds to do the search", elapsed)

    log.info("Best x, y: %s", best)


if __name__ == '__main__':
    cli = argparse.ArgumentParser()

    cli.add_argument(
        '-d', '--database',
        required=False,
        default='distributed_test'
    )

    cli.add_argument(
        '-p', '--parallel',
        action='store_true'
    )

    cli.add_argument(
        '-o', '--objective',
        type=Objective,
        choices=list(Objective),
        default=Objective.ROSENBROCK
    )

    cli.add_argument(
        '-a', '--algorithm',
        type=Algorithm,
        choices=list(Algorithm),
        default=Algorithm.RANDOM
    )

    cli.add_argument(
        '-s', '--startup-jobs',
        type=int,
        default=200
    )

    cli.add_argument(
        '-m', '--max-evals',
        type=int,
        default=400
    )

    cli.add_argument(
        '-L', '--log-file',
        required=True
    )

    main(cli.parse_args())
