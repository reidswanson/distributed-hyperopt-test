"""
Documentation
"""
# Python Modules
import logging
import math
import time

# 3rd Party Modules

# Project Modules


def rosenbrock(params):
    x = [params[k] for k in sorted(params.keys(), key=lambda v: int(v[1:]))]

    f_o = 0
    f_x = 0
    for i in range(len(x) - 1):
        x0, x1 = x[i], x[i+1]
        f_x += 100.0 * (x1 - x0**2)**2 + (1.0 - x0)**2

    log = logging.getLogger(__name__)
    log.info(
        "f(x) = % 4.3f |f - f'| = % 4.3f",
        f_x, abs(f_x - f_o)
    )

    return f_x


def eggholder(params):
    x, y = params['x'], params['y']

    a = (y + 47)
    b = math.sin(math.sqrt(abs((0.5 * x + (y + 47)))))
    c = x * math.sin(math.sqrt(abs(x - (y + 47))))
    f_xy = -a * b - c
    f_opt = -959.6406627106155

    log = logging.getLogger(__name__)
    log.info(
        "x = % 4.3f y = % 4.3f f(x, y) = % 4.3f |f - f'| = % 4.3f",
        x, y, f_xy, abs(f_xy - f_opt)
    )

    return f_xy
